$(document).ready(function() {
    var selectedcolumn;

// AddItem form open
    $('.add_data').click(function () {
        $('#itemModal').modal('show');
        $('#item_form')[0].reset();
        $('#form_output').html('');
        $('#button_action').val('insert');
        $('#action').val('Add');
        selectedcolumn = $(this).closest('.column').attr('id');
    });

//Add Item functionality
    $('#action').click(function (event) {
        event.preventDefault();
        var title = $('#title').val();
        var description = $('#description').val();


        $.ajax({
            url: '/AddItem',
            type: 'post',
            data: {_token: CSRF_TOKEN, title: title, description: description, column: selectedcolumn, projectid: currentprojectid},
            success: function (response) {
                // alert("success: " + response);
                //  var div = document.createElement("div");
                //  div.className = "portlet";
                //  div.innerHTML = response.title;
                //  document.getElementById('#1').appendChild(div);

                // $('#title').val('');
                //  $('#description').text('');
                location.reload();

                // ooit de bedoeling om via ajax div toe te voegen aan view.
            },
            error: function (xhr, status, error) {
                alert("AddItem error: " + xhr.responseText);
            }
        });

    });

});
