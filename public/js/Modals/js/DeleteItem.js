$(document).ready(function() {

    $('.delete_item').click(function (event) {
        event.preventDefault();
        var itemid = $(this).data('itemid');
        this.closest('.portlet').remove(); // verwijderd item uit pagina daarna uit database.
        $.ajax({
            url: '/DeleteItem',
            type: 'post',
            data: {_token: CSRF_TOKEN, itemid: itemid},
            success: function (response) {

            },
            error: function (xhr, status, error) {
                alert("DeleteItem error: " + xhr.responseText);
            }
        });

    });

});
