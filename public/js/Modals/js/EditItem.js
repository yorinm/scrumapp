$(document).ready(function() {
    var edititemid;
// EditItem form open
    $('.edit_item').click(function () {
        $('#EditItemModal').modal('show');
        $('#EditItem_form')[0].reset();
        $('#EditItemform_output').html('');
        $('#EditItembutton_action').val('insert');
        $('#actionEditItem').val('Add');
        edititemid = $(this).data('itemid');
        $.ajax({
            url: '/RequestItemData',
            type: 'post',
            data: {_token: CSRF_TOKEN,itemid: edititemid},
            success: function (response) {
            $('#EditItemtitle').val(response.title);
                $('#EditItemdescription').val(response.description);
            },
            error: function (xhr, status, error) {
                alert("requestitem error: " + xhr.responseText);
            }
        });
    });

//EditItem functionality
    $('#EditItemaction').click(function (event) {
        event.preventDefault();
        var itemid = edititemid;
        var title = $('#EditItemtitle').val();
        var description = $('#EditItemdescription').val();
        var editbutton = $(this);
        //console.log(edititemid, title,  description);
        $.ajax({
            url: '/EditItem',
            type: 'post',
            data: {_token: CSRF_TOKEN, itemid: itemid, title: title, description: description},
            success: function (response, xhr) {
                var portlet = find('edititem' + itemid);
                //$('.portlet').find('#edit_item' + itemid).parent().parent().find('.itemtitle').text.innerText = title;
                $('.portlet').find('#edit_item' + itemid).parent().parent().find('.itemtitle').html(title);
                $('.portlet').find('#edit_item' + itemid).parent().parent().parent().find('.portlet-content').html(description);
                $(this).find('.portlet-content').innerHTML = description;
                //alert( $(this).find('.itemtitle').val());
                $('#EditItemModal').modal('hide');
            },
            error: function (xhr, status, error) {
                alert("EditItem error: " + xhr.responseText);
            }
        });
    });
});
