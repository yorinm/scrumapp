$(document).ready(function() {
// addColumn form open
    $('.add_column').click(function () {
        $('#ColumnModal').modal('show');
        $('#Column_form')[0].reset();
        $('#Columnform_output').html('');
        $('#Columnbutton_action').val('insert');
        $('#actionColumn').val('Add');
    });

//AddColumn functionality
    $('#actionColumn').click(function (event) {
        event.preventDefault();
        var title = $('#Columntitle').val();

        $.ajax({
            url: '/AddColumn',
            type: 'post',
            data: {_token: CSRF_TOKEN, title: title, projectid: $('meta[name="projectid"]').attr('content')},
            success: function (response) {
                // alert("success: " + response);
                //  var div = document.createElement("div");
                //  div.className = "portlet";
                //  div.innerHTML = response.title;
                //  document.getElementById('#1').appendChild(div);

                // $('#title').val('');
                //  $('#description').text('');
                location.reload();

                // ooit de bedoeling om via ajax div toe te voegen aan view.
            },
            error: function (xhr, status, error) {
                alert("AddColumn error: " + xhr.responseText);
            }
        });

    });
});
