$( document ).ready(function() {
var oldColumn;

    $(".column").sortable({
        connectWith: ".column",
        handle: ".portlet-header",
        cancel: ".portlet-toggle",
        start: function (event, ui) {
            ui.item.addClass('tilt');
            oldColumn = ui.item.parent().data('columnid');
        },
        update: function(event, ui) {
        },
        stop: function (event, ui) {
            ui.item.removeClass('tilt');
            var newColumn = ui.item.parent().data('columnid');
            var itemid = ui.item.data('itemid');
            var list = [];
            $('.column').find('.portlet').each(function(){
                if($(this).parent().data('columnid') === newColumn){
                    list.push($(this).data('itemid'));
                }
            });


            $.ajax({
                type: "post",
                url: "/MoveItem",
                data: {newColumn: newColumn, itemid: itemid, itemlist: list },
                success: function(response){
                    //alert("Data Save: " + response);
                },
                error: function(xhr, status, error){
                    alert(xhr.responseText);
                }
            });
        }
    });

    $(".portlet")
        .addClass("ui-widget ui-widget-content ui-helper-clearfix ui-corner-all")
        .find(".portlet-header")
        .addClass("ui-widget-header ui-corner-all")
        .prepend("<img src='/image/expand-button.svg' class='expandbutton portlet-toggle' alt='edit'/>");

    $(".portlet-toggle").click(function () {
        var icon = $(this);
        icon.toggleClass("ui-icon-minusthick ui-icon-plusthick");
        icon.closest(".portlet").find(".portlet-content").toggle();
    });

});
