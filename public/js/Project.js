$(document).ready(function() {

    $('.UserButton').click(function () {
        //Update database
        //update button color
        //update class name(AddUser/RemoveUser)
        var userid = $(this).data('userid');
        var button = $(this);
        if($(this).hasClass('RemoveUser')){
        $.ajax({
            url: '/ProjectDashboard/'+ currentprojectid +'/RemoveUserFromProject',
            type: 'post',
            data: {_token: CSRF_TOKEN, userid:userid, projectid: currentprojectid},
            success: function (response, xhr) {
                button.removeClass("RemoveUser btn-danger");
                button.addClass("AddUser btn-success");
                button.text("Toevoegen");
                //update button color and class
                //alert('Removeuser' + response + xhr.responseText)
            },
            error: function (xhr, status, error) {
                alert("ChangeProjectUser error: " + xhr.responseText);
            }
        });}
        else{
            $.ajax({
                url: '/ProjectDashboard/'+ currentprojectid +'/AddUserToProject',
                type: 'post',
                data: {_token: CSRF_TOKEN, userid:userid, projectid: currentprojectid},
                success: function (response, xhr) {
                    button.removeClass("AddUser btn-success");
                    button.addClass("RemoveUser btn-danger");
                    button.text("Verwijderen");
                    //update button color and class
                      //alert("Adduser" + response  + xhr.responseText);
                },
                error: function (xhr, status, error) {
                    alert("ChangeProjectUser error: " + xhr.responseText);
                }
            });
        }
    });
});
