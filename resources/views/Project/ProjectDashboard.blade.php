@extends('layouts.app')
@section('content')

    <!--liijst vna gebruikers die je kan selecteren voor je project, daarna een save knop.
    rond controller wss een loop die het in de DB gooit. -->
    <div class="container">
        <div class="row">
        <div class="card col-6">
            <div class="card-header">Project Titel Aanpassen</div>
            <div class="card-body">
                <form action="/ProjectDashboard/{{request()->route('project')}}/EditProjectTitle" method="post">
                    @csrf
                        <input type="text" name="projecttitle" id="projecttitle" value="{{$projects->title}}">
                    <button type="submit" class="btn btn-success">Aanpassen</button>
                </form>
            </div>
        </div>
        <div class="card col-6">
            <div class="card-header">Project Verwijderen</div>
            <div class="card-body"><button class="btn btn-danger">Project Verwijderen</button></div>
        </div>
        </div>
    </div>
    <div class="container">
        <div class="card">
            <div class="card-header">Gebruiker toevoegen aan project</div>
                <div class="card-body">
                    <div class="row">
    @foreach($users as $user)

        <div class="card col-2 nopadding">
            @php
                $projectusers = \App\ProjectUser::GetUsersForProject(Request()->project);
                $users = [];
            @endphp
            @foreach($projectusers as $projectuser)
                @php
                    array_push($users, $projectuser->userid);
                @endphp
            @endforeach
            @php

                @endphp
            <div class="card-header">
                <h5 class="nopadding">{{$user->name}}</h5>
            </div>
            <div class="card-body">
                @if(in_array($user->id,$users))
                    <button class="btn btn-danger RemoveUser UserButton" data-userid="{{$user->id}}">Verwijderen</button>
                @else
                    <button class="btn btn-success AddUser UserButton" data-userid="{{$user->id}}">Toevoegen</button>
                @endif
            </div>
        </div>
    @endforeach
                </div>
            </div>
        </div>
    </div>

    <div class="container">

    </div>




@endsection
