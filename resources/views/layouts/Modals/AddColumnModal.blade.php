<div id="ColumnModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="post" id="Column_form">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Column</h4>
                </div>
                <div class="modal-body">
                    @csrf
                    <span id="Columnform_output"></span>
                    <div class="form-group">
                        <label for="title">Enter Title</label>
                        <input type="text" name="title" id="Columntitle" class="form-control"/>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="button_action" id="Columnbutton_action" value="insert" />
                    <input type="submit" name="submit" id="actionColumn" value="add" class="btn btn-info action"/>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
