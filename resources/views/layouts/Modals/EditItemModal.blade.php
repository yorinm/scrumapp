<div id="EditItemModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="post" id="EditItem_form">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Item</h4>
                </div>
                <div class="modal-body">
                    @csrf
                    <span id="EditItemform-output"></span>
                    <div class="form-group">
                        <label for="title">Enter Title</label>
                        <input type="text" name="EditItemtitle" id="EditItemtitle" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label for="description">Enter Description</label>
                        <textarea cols="50" rows="5" name="EditItemdescription" id="EditItemdescription" class="form-control"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="EditItembutton_action" id="EditItembutton_action" value="insert" />
                    <input type="submit" name="submit" id="EditItemaction" value="Edit" class="btn btn-info action"/>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
