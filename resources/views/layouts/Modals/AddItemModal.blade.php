<div id="itemModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="post" id="item_form">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Item</h4>
                </div>
                <div class="modal-body">
                    @csrf
                    <span id="form-output"></span>
                    <div class="form-group">
                        <label for="title">Enter Title</label>
                        <input type="text" name="title" id="title" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label for="description">Enter Description</label>
                        <textarea cols="50" rows="5" name="description" id="description" class="form-control"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="button_action" id="button_action" value="insert" />
                    <input type="submit" name="submit" id="action" value="add" class="btn btn-info action"/>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
