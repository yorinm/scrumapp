@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <select id="projectselector">
                        @foreach($projects as $project)
                            @if($project->id == Request()->project)
                            <option value="{{$project->id}}" selected>{{$project->title}}</option>
                            @else
                            <option value="{{$project->id}}">{{$project->title}}</option>
                            @endif
                        @endforeach
                    </select>
                    @php
                        $column = "";
                    @endphp
                        <div class="columnrowcontainer">
                        @foreach ($columns as $column)
                        <div class="columncontainer">
                            <span class="columnspan">{{$column->title}}</span>
                                <div class="column" data-columnid="{{$column->id}}" id="{{$column->id}}">
                                    <button type="button" name="add" id="add_data" class ="btn btn-success btn-sm add_data">Add</button>
                                    @php
                                    $columnitems = \App\Item::getItemsPerColumn($column->id);

                                    @endphp

                                    @foreach ($columnitems as $item)

                                            <div class="portlet" data-itemid="{{$item->id}}">
                                                <div class="portlet-header row">
                                                    <div class="col-8 itemtitle">
                                                    {{ $item->title }}
                                                    </div>
                                                    <div class="col-2 buttoncontainer">
                                                        <button type="button" name="edit" id="edit_item{{$item->id}}" data-itemid="{{$item->id}}" class ="btn btn-success btn-sm edit_item"><img src="/image/edit.svg" alt="edit"/></button>
                                                        <button type="button" name="delete" id="delete_item" data-itemid="{{$item->id}}" class ="btn btn-success btn-sm delete_item"><img src="/image/rubbish-bin-delete-button.svg" alt="delete"/></button>
                                                    </div>
                                                </div>
                                                <div class="portlet-content">{{$item->description}}</div>
                                            </div>
                                    @endforeach
                                </div>
                            </div>
                        @endforeach
                        </div>
                            <div class="newColumn">
                                <button type="button" name="addColumn" id="add_column" class="btn btn-success btn-sm add_column">Add column</button>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('layouts.Modals.AddColumnModal')
@include('layouts.Modals.AddItemModal')
@include('layouts.Modals.EditItemModal')
@include('layouts.Modals.DeleteItemModal')

@endsection
