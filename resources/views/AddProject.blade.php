@extends('layouts.app')
@section('content')


<div class="content">
    <div class="row justify-content-center">
        <div class="col-8">
            <div class="card">
                <div class="card-header">Project Toevoegen</div>
                <div class="card-body">
                    <form method="post" action="/InsertProject">
                        @csrf
                        <div class="form-group">
                        <label>Project Naam:</label>
                        <input type="text" name="title" id="title">
                        </div>
                        <div class="form-group">
                        <input type="submit" value="Toevoegen" class="btn btn-success">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>





@endsection
