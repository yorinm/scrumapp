@extends('layouts.app')
@section('content')

<!--liijst vna gebruikers die je kan selecteren voor je project, daarna een save knop.
    rond controller wss een loop die het in de DB gooit. -->
    @foreach($users as $user)

        <div class="card col-2 nopadding">
            @php
                $projectusers = \App\ProjectUser::GetUsersForProject(Request()->project);
                $users = [];
                @endphp
                @foreach($projectusers as $projectuser)
                    @php
                        array_push($users, $projectuser->userid);
                    @endphp
                @endforeach
            @php

            @endphp
            <div class="card-header">
                <h5 class="nopadding">{{$user->name}}</h5>
            </div>
            <div class="card-body">
               @php
                if(in_array($user->id, $users))
               echo'<button class="btn btn-success">Verwijderen</button>';
                else
                    echo'<button class="btn btn-danger">Toevoegen</button>';
               @endphp
            </div>
        </div>


    @endforeach





@endsection
