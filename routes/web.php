<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();

Route::get('/Project/{project}/Dashboard', 'HomeController@index')->name('dashboard');
Route::get('/AddProject', 'HomeController@AddProject')->name('AddProject');


Route::post('/MoveItem', 'HomeController@MoveItem');
Route::post('/AddItem', 'HomeController@AddItem');
Route::post('/AddColumn', 'HomeController@AddColumn');
Route::post('/EditItem', 'HomeController@EditItem');
Route::post('/DeleteItem', 'HomeController@DeleteItem');
Route::post('InsertProject', 'HomeController@InsertProject');
Route::post('/RequestItemData', 'HomeController@RequestItemData');


Route::get('/ProjectDashboard/{project}/', 'ProjectController@index');
Route::post('/ProjectDashboard/{project}/AddUserToProject', 'ProjectController@AddUserToProject');
Route::post('/ProjectDashboard/{project}/RemoveUserFromProject', 'ProjectController@RemoveUserFromProject');
Route::post('/ProjectDashboard/{project}/EditProjectTitle', 'ProjectController@EditProjectTitle');
