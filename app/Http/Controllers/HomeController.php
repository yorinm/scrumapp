<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateBacklogItemRequest;
use App\Http\Requests\CreateColumnRequest;
use App\Project;
use App\ProjectUser;
use App\User;
use Illuminate\Http\Request;
use App\Item;
use App\column;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index($project)
    {
        $columns = column::all()->where('projectid','=', $project);
        $projects = $this->GetProjectsFromUser(auth()->user()->id);
        if(auth()->user()->latestprojectid != $project){
            User::EditLatestPRojectId($project, auth()->user()->id);
        }
        return view('/dashboard', compact( 'columns', 'projects'));
    }
    public function AddProject(){
        return view('/AddProject');
    }
    public function AddUserToProject(){
        $users = User::all();

        return view('/AddUserToProject', compact('users'));
    }

    public function MoveItem(Request $request){
        Item::UpdateItemLocation($request->get('itemid'), $request->get('newColumn'));
        $data = $request->get('itemlist');
        $counter = 1;
        foreach($data as $val){
            Item::ReOrderItem($val,$counter);
            $counter++;
        }
    }

    public function AddItem(CreateBacklogItemRequest $request){
       $title = $request->input('title');
       $description = $request->input('description');
       $column = $request->input('column');
       $projectid = $request->input('projectid');

       $data = array('title'=>$title, 'description'=>$description, 'column'=>$column, 'projectid'=>$projectid);

       $value = Item::InsertItem($data);
           echo json_encode($data);
    exit;
    }
    public function AddColumn(CreateColumnRequest $request){
        $Columntitle = $request->input('title');

        $data = array('title'=>$Columntitle, 'projectid'=>$request->input('projectid'));

        $value = column::InsertColumn($data);
        echo json_encode($data);
        exit;
    }
    public function EditItem(Request $request){
       Item::UpdateItem($request->get('itemid'), $request->get('title'), $request->get('description'));
    }

    public function DeleteItem(Request $request){
        Item::DeleteItem($request->get('itemid'));
    }

    public function GetProjectsFromUser($userid){
        return ProjectUser::GetProjectListByUser($userid);
    }

    public function InsertProject(Request $request){
       $lastid = Project::InsertProject($request->input('title'));
        ProjectUser::InsertProject(auth()->user()->id, $lastid);
        $temp = ProjectUser::LastAddedProject(auth()->user()->id);
        $temp2 = $temp->toArray();
        $newproject = (array)$temp2[0];
        //dd($newproject);
        return redirect()->route('dashboard', ['project' => $newproject['projectid']]);
    }

    public function RequestItemData(Request $request){
        $temp =  Item::getItemdata($request->input('itemid'));
        $temp->toArray();
        return (array)$temp[0];
    }


}
