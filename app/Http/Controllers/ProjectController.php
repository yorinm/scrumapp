<?php

namespace App\Http\Controllers;

use App\Project;
use App\ProjectUser;
use App\User;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    public function index(Request $request){
        $users = User::all();
        $projects = Project::Find($request->route('project'));
        return view('/Project/ProjectDashboard', compact('users', 'projects'));
    }

    public function AddUserToProject(Request $request){
        ProjectUser::AddUserToProject($request->userid,$request->projectid);
    }

    public function RemoveUserFromProject(Request $request){
        return ProjectUser::RemoveUserFromProject($request->userid,$request->projectid);
    }
    public function EditProjectTitle(Request $request){
        Project::EditProjectTitle(request()->route('project'), $request->projecttitle);
        return redirect()->back();
    }
}
