<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class Project extends Model
{
    public static function InsertProject($projecttitle){
        return DB::table('projects')->insertGetId(
            ['title' => $projecttitle]
        );
            // bedenken hoe ik project id gelijk kan houden maar wel kan optellen met een nieuwe. zonder auto increment.
        // wss iets met get highest projectid en dan optellen. hierdoor komen uitiendelijk alle mensnen van het zelfde project in 1 projectid.
    }
    public static function EditProjectTitle($projectid, $projecttitle){
        DB::table('projects')->where('id',$projectid)->update(['title'=> $projecttitle]);
    }

}
