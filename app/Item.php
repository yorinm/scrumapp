<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\DocBlock\Description;


class Item extends Model
{
    //

    public static function getItemsPerColumn($id){
        $items = DB::table('items')->where('columnid','=', $id)->orderBy('itemorder', 'asc')->get();
        return $items;
    }

    public static function UpdateItemLocation($itemId, $newColumnId){
        DB::table('items')->where('id' , $itemId)
            ->update(['columnid' => $newColumnId]);
    }

    public static function UpdateItem($itemid, $itemtitle, $itemdescription){
        DB::table('items')->where('id', $itemid)
            ->update(['title' => $itemtitle, 'description' => $itemdescription]);
    }

    public static function ReOrderItem($itemId, $order){
        DB::table('items')->where('id', $itemId)
            ->update(['itemorder' => $order]);
    }

    public static function InsertItem($data){

        DB::table('items')->insert(
            ['title' => $data['title'], 'columnid' => $data['column'], 'description' =>$data['description'], 'projectid'=>$data['projectid']]
        );
        // insert
       // echo $itemtitle;
    }



    public static function DeleteItem($id){
        DB::table('items')->where('id', $id)->delete();
    }

    public static function getItemdata($id){
        return DB::table('items')->where('id', '=', $id)->get();
    }


}
