<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ProjectUser extends Model
{
    public static function GetProjectListByUser($userid){
        return DB::table('projectusers')
            ->join('projects', 'projectusers.projectid', '=', 'projects.id')
            ->where('userid', '=', $userid)->get();
    }

    public static function InsertProject($userid, $projectid){
        DB::table('projectusers')
            ->insert([
                'userid' => $userid,
                'projectid' => $projectid
            ]);
    }
    public static function LastAddedProject($userid){
        return DB::table('projectusers')
            ->where('userid', '=', $userid)
            ->orderBy('projectid', 'DESC')
            ->limit(1)->get();
    }

    public static function GetUsersForProject($projectid){
       return DB::table('projectusers')
                ->select('userid')
                ->where('projectid', '=', $projectid)
                ->get();
       // give all users as array, then find user with every card. compare id's
    }
    public static function AddUserToProject($userid,$projectid){
        DB::table('projectusers')->insert(
            ['userid' => $userid, 'projectid' => $projectid]
        );
    }
    public static function RemoveUserFromProject($userid,$projectid){
        DB::table('projectusers')
                ->where('userid', $userid)->where('projectid',$projectid)->delete();
    }

}
