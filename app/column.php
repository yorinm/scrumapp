<?php

namespace App;

use App\Http\Requests\CreateColumnRequest;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class column extends Model
{

    public static function InsertColumn($data){

        DB::table('columns')->insert(
            ['title' => $data['title'], 'projectid' => $data['projectid']]
        );
    }
}
