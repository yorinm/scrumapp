<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Item;
use Faker\Generator as Faker;

$factory->define(Item::class, function (Faker $faker) {
    return [
        'columnid' => $faker->numberBetween(0,7),
        'itemorder' => $faker->numberBetween(1,1),
        'projectid' => $faker->numberBetween(1,1),
        'title' => $faker->sentence(4, 1),
        'description' => $faker->text(100),
        'storypoint' => $faker->numberBetween(1,40),

    ];
});
