<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\column;
use Faker\Generator as Faker;

$factory->define(column::class, function (Faker $faker) {
    return [
        'title' => $faker->word,
        'projectid' => $faker->numberBetween(1,1)
    ];
});
