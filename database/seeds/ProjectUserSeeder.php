<?php

use Illuminate\Database\Seeder;

class ProjectUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('projectusers')->insert([
            'userid' => 1,
            'projectid' => 1,
        ]);
        DB::table('projectusers')->insert([
            'userid' => 1,
            'projectid' => 2,
        ]);
    }
}
