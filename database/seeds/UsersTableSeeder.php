<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert([
            'name' => 'Admin',
            'latestprojectid' => 1,
            'email' => 'yorin.meijerink@gmail.com',
            'password' => bcrypt('adminadmin'),
        ]);
    }
}
