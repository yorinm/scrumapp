<?php

use Illuminate\Database\Seeder;

class ColumnstableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\column::class, 8)->create();
    }
}
