<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProjectstableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('projects')->insert([
            'title' => 'ScrumApp DashBoard',
        ]);
        DB::table('projects')->insert([
            'title' => '2e Scrumapp',
        ]);
    }
}
